import { Injectable } from '@angular/core';
import { Quiz} from './quiz.model';

@Injectable({
  providedIn: 'root'
})
export class QuizService {
  quizzes: Quiz []= [
    {
      question: 'Cual es la capital de Somalia',
      answer: [
        { option: 'Nairobi', correct: false},
        { option: 'Asmara', correct: false},
        { option: 'Mogadisk', correct: true }
       
      ]
    },
    {
      question: 'Cual es la capital de Turquia',
      answer: [
        { option: 'Nairobi', correct: false},
        { option: 'Chipre', correct: false},
        { option: 'Estambul', correct: true }
       
      ]
    },
    {
      question: 'Cual es la capital de Belice',
      answer: [
        { option: 'Tijuana', correct: false},
        { option: 'Quito', correct: false},
        { option: 'Belmopan', correct: true }
       
      ]
    },
    {
      question: 'Cual es la capital de Canada',
      answer: [
        { option: 'Montereal', correct: false},
        { option: 'Quebec', correct: false},
        { option: 'Ottawa', correct: true }
       
      ]
    },
    {
      question: 'Cual es la capital de Mexico',
      answer: [
        { option: 'Guadalajara', correct: false},
        { option: 'Mecico City', correct: true},
        { option: 'Chiapas', correct: false }
       
      ]
    },
    {
      question: 'Cual es la capital de Groenlandia',
      answer: [
        { option: 'Narsaq', correct: false},
        { option: 'Nuuk', correct: true},
        { option: 'Igaliku', correct: false }
       
      ]
    },
    {
      question: 'Cual es la capital de Islandia',
      answer: [
        { option: 'Reikiavik', correct: true},
        { option: 'Selfoss', correct: false},
        { option: 'Akranes', correct: false }
       
      ]
    },
    {
      question: 'Cual es la capital de Hungria',
      answer: [
        { option: 'Debrecen', correct: false},
        { option: 'Kosice', correct: false},
        { option: 'Budapest', correct: true }
       
      ]
    },
    {
      question: 'Cual es la capital de Republica Chequia',
      answer: [
        { option: 'Brno', correct: false},
        { option: 'Praga', correct: true},
        { option: 'Monaco', correct: false }
       
      ]
    },
    {
      question: 'Cual es la capital de Escocia',
      answer: [
        { option: 'Edimburgo', correct: true},
        { option: 'Isla de Man', correct: false},
        { option: 'Kelso', correct: false }
       
      ]
    },
  ]
  constructor() { }

  getQuizzes (){
    return this.quizzes;
  }
}
