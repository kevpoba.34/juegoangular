import { Component, OnInit } from '@angular/core';
import { Quiz} from '../quiz.model';
import {QuizService }  from '../quiz.service';


@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit {
quizzes: Quiz[] = [];

currentQuiz =0;
answerSelected =false;
correctAnswer =0;
incorrectAnswer = 0;
title = 'Tiempo Transcurido';

    interval;
    time = new Date(null);
randomize: number;

result = false;

  constructor( private quizService: QuizService) { }

  ngOnInit(): void {
      this.quizzes = this.quizService.getQuizzes();

      this.randomize = Math.floor(Math.random() * this.quizzes.length );
  }

  onAnswer(option: boolean){
        console.log(option);
        this.answerSelected=true;
       
        setTimeout( () => {
          this.currentQuiz++;
          this.randomize = Math.floor(Math.random() * this.quizzes.length );
          this.answerSelected = false;
        },3000);
        
        if (option){
          this.correctAnswer++;
        }else{
          this.incorrectAnswer++;
        }
  }
  
  showResult(){
    this.result = true;
  }
  

    startTimer(){
      this.interval = setInterval(() =>{
      this.time.setSeconds(this.time.getSeconds() + 1);
      }, 1000);
    }

    pauseTimer (){
        clearInterval(this.interval);
    }

    resetTimer(){
      this.time.setSeconds(0);
    }

}
