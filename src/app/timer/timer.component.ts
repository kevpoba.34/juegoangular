import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css']
})
export class TimerComponent implements OnInit {
    title = 'Tiempo Transcurido';

    interval;
    time = new Date(null);

    startTimer(){
      this.interval = setInterval(() =>{
      this.time.setSeconds(this.time.getSeconds() + 1);
      }, 1000);
    }

    pauseTimer (){
        clearInterval(this.interval);
    }

    resetTimer(){
      this.time.setSeconds(0);
    }

  constructor() { }

  ngOnInit(): void {
  }

}
